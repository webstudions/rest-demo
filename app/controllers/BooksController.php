<?php

class BooksController extends \BaseController {

	private function rules($id=0) {
		return array(
			'title'	=>	array('required')
		);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($uid)
	{	
		$books = Book::leftJoin('users', 'users.id', '=', 'books.user_id')
		->whereUserId($uid)
		->select('books.id', 'books.title', 'users.id', 'users.name', 'users.email')
		->get();
		return Response::json($books);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return 'You can do a POST request to /users/{user}/books to add a new book.';
	}


	/**
	 * Store a newly created resource in storage.
	 * 
	 * @param  int  $uid,
	 * @return Response
	 */
	public function store($uid)
	{
		$input = Input::all();
		$validator = Validator::make($input, $this->rules());
		$user = User::find($uid);

		if($user != null) {
			if($validator->passes()) {
				$book = new Book();
				$book->title = $input['title'];
				$book->user_id = $uid;
				$book->save();
				return Response::json(array('status' => true, 'message' => 'Book of user with ID='.$uid.' was saved successfully with ID='.$book->id));
			} else {
				return Response::json(array('status' => false, 'message' => $validator->messages()));
			}
		} else {
			return Response::json(array('status' => false, 'message' => 'No user with ID='.$id.' could be found.'));
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $uid, $bid
	 * @return Response
	 */
	public function show($uid, $bid)
	{
		$book = Book::find($bid);
		if($book != null)
			return Response::json($book);
		else 
			return Response::json(array('status' => false, 'message' => 'No book with ID='.$bid.' could be found.'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $uid, $bid
	 * @return Response
	 */
	public function edit($uid, $bid)
	{
		return 'You can do a PUT request to /users/{user}/books/{book} to edit an existing user.';
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $uid, $bid
	 * @return Response
	 */
	public function update($uid, $bid)
	{
		$input = Input::all();
		$validator = Validator::make($input, $this->rules());
		$user = User::find($uid);
		$book = Book::find($bid);

		if($user != null && $book != null) {
			if($validator->passes()) {
				$book->title = $input['title'];
				$book->user_id = $uid;
				$book->save();
				return Response::json(array('status' => true, 'message' => 'Book of user with ID='.$uid.' was edit successfully with ID='.$bid));
			}
		} else {
			return Response::json(array('status' => false, 'message' => 'User or book could not be found.'));
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $uid, int $bid
	 * @return Response
	 */
	public function destroy($uid, $bid)
	{
		$user = User::find($uid);
		$book = Book::find($bid);

		if($user != null && $book != null) {
			$book->delete();
			return Response::json(array('status' => true, 'message' => 'Book with ID='.$bid.' was deleted successfully.'));
		} else {
			return Response::json(array('status' => false, 'message' => 'User or book could not be found.'));	
		}
	}


}
