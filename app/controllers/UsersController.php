<?php

class UsersController extends \BaseController {

	private function rules($id=0) {
		return array(
			'name'	=>	array('required'),
			'email' => array('required', 'unique:users,email'.($id ? ",$id" : '')),
			'password'	=>	array('required'),
			'password_confirm'	=>	array('required', 'same:password')
		);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Response::json(User::all());
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return 'You can do a POST request to /users to save a new user.';
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validator = Validator::make($input, $this->rules());

		if($validator->passes()) {
			$user = new User();
			$user->name = $input['name'];
			$user->password = Hash::make($input['password']);
			$user->email = $input['email'];
			$user->save();
			return Response::json(array('status' => true, 'message' => 'User was saved successfully with ID='.$user->id));
		} else {
			return Response::json(array('status' => false, 'message' => $validator->messages()));
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::find($id);
		if($user != null) 
			return Response::json($user);
		else
			return Response::json(array('status' => false, 'message' => 'No user with ID='.$id.' could be found.'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return 'You can do a PUT request to /users to edit an existing user.';
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
		$validator = Validator::make($input, $this->rules($id));
		$user = User::find($id);

		if($validator->passes() && $user != null) {
			$user->name = $input['name'];
			$user->password = Hash::make($input['password']);
			$user->email = $input['email'];
			$user->save();
			return Response::json(array('status' => true, 'message' => 'User with ID='.$user->id.' was updated successfully.'));
		} else {
			if($user == null) {
				return Response::json(array('status' => false, 'message' => 'This user doesn\'t exist.'));
			} else {
				return Response::json(array('status' => false, 'message' => $validator->messages()));
			}			
		}	
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::find($id);

		if($user != null) {
			$user->delete();
			return Response::json(array('status' => true, 'message' => 'User with ID='.$id.' was deleted successfully.'));
		} else {
			return Response::json(array('status' => false, 'message' => 'No user with ID='.$id.' could be found.'));	
		}
	}


}
