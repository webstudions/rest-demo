<?php

class Book extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'books';

	public function user() {
		$this->belongsTo('User');
	}

}